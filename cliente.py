from tcp import *
from time import *

serverIP = "209.165.209.2"
serverPort = 1234

client = TCPClient()

def onTCPConnectionChange(type):
	print("conectado a " + client.remoteIP() + " cambiado a estado " + str(type))

def onTCPReceive(data):
	print("recivido de " + client.remoteIP() + " con los datos: " + data)

def main():
	client.onConnectionChange(onTCPConnectionChange)
	client.onReceive(onTCPReceive)

	print(client.connect(serverIP, serverPort))

	count = 0
	while True:
		count += 1
		data = "hola " + str(count)
		client.send(data)

		sleep(5)

if __name__ == "__main__":
	main()