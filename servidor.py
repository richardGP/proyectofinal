from tcp import *
from time import *

port = 1234
server = TCPServer()

def onTCPNewClient(client):
	def onTCPConnectionChange(type):
		print("conexion a " + client.remoteIP() + " cambiadoa a estado " + str(type))
		
	def onTCPReceive(data):
		print("recibido de " + client.remoteIP() + " con los datos: " + data)
		# send back same data
		client.send(data)

	client.onConnectionChange(onTCPConnectionChange)
	client.onReceive(onTCPReceive)

def main():
	server.onNewClient(onTCPNewClient)
	print(server.listen(port))

	# don't let it finish
	while True:
		sleep(3600)

if __name__ == "__main__":
	main()